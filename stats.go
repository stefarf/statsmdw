package statsmdw

import (
	"net/http"
	"sync"
	"time"

	"github.com/labstack/echo/v4"
)

type Stats struct {
	sync.Mutex
	count     uint
	tStart    time.Time
	tStop     time.Time
	tpsValues []float64

	tpsSize   int
	tpsPeriod time.Duration
}

func New(tpsSize int, tpsPeriod time.Duration) *Stats {
	s := &Stats{
		tpsValues: make([]float64, tpsSize),
		tpsSize:   tpsSize,
		tpsPeriod: tpsPeriod,
	}
	s.reset(time.Now())
	return s
}

func (s *Stats) Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		now := time.Now()

		s.Lock()
		s.count++
		if now.After(s.tStop) {
			secs := float64(now.Sub(s.tStart)) / float64(time.Second)
			s.insertTps(float64(s.count) / secs)
			s.reset(now)
		}
		s.Unlock()

		return next(c)
	}
}

func (s *Stats) GetStatsHandler(c echo.Context) error {
	return c.JSON(http.StatusOK, s.tpsValues)
}

func (s *Stats) reset(now time.Time) {
	s.count = 0
	s.tStart = now
	s.tStop = now.Add(s.tpsPeriod)
}

func (s *Stats) insertTps(tps float64) {
	if len(s.tpsValues) < s.tpsSize {
		s.tpsValues = append(s.tpsValues, tps)
	} else {
		l := len(s.tpsValues)
		i, j := 0, 1
		for {
			if j == l {
				break
			}
			s.tpsValues[i] = s.tpsValues[j]
			i, j = i+1, j+1
		}
		s.tpsValues[l-1] = tps
	}
}
